import Vue from 'vue'
import router from './router/index.js'
import store from './store/index.js'
import vuetify from './plugins/vuetify.js';
import App from './App.vue'
// import 'material-design-icons-iconfont/dist/material-design-icons.css'

Vue.config.productionTip = false;

new Vue({
  router,
  store,
  vuetify,
  render: h => h(App)
}).$mount('#app')
